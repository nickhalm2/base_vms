#!/bin/bash 

set -eux

if [[ ! "$GOLANG" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
	exit
fi

#get and install golang
echo "==>Starting the golang install"
go_version=1.7
gbash=/etc/profile.d/golang.sh
gsha256sum=702ad90f705365227e902b42d91dd1a40e48ca7f67a2f4b2fd052aaa4295cd95

echo "${gsha256sum} golang.tar" > golang.tar-CHECKSUM
wget https://storage.googleapis.com/golang/go${go_version}.linux-amd64.tar.gz -q -O golang.tar
rslt=$(sha256sum -c golang.tar-CHECKSUM 2>&1 | grep OK | wc -l)

if [ ${rslt} = 1 ]; then
	tar -C /usr/local -xzf golang.tar
	echo "==>Adding the golang path to user path"
	echo "export PATH=/usr/local/go/bin:\$PATH" > $gbash
else
	echo "==>golang sha256 did not match, skipping golang install"
fi
rm -f golang.tar


