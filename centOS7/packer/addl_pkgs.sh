#!/bin/bash -eux

echo "==>Adding additional packages"
yum install -y sudo epel-release dos2unix gcc make curl wget bzip2 openssl-devel mlocate vim-enhanced telnet tmux unzip git postgresql 
