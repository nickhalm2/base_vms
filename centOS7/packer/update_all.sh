#!/bin/bash

set -x

#create the docker group so non root members can have access to docker
groupadd docker

yum update -y
yum install -y sudo docker epel-release dos2unix gcc make curl wget bzip2 openssl-devel mlocate vim-enhanced telnet tmux unzip git postgresql 


# sudo
echo "vagrant        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers.d/vagrant
sed -i "s/^.*requiretty/#Defaults requiretty/" /etc/sudoers
