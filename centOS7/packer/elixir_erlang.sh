#!/bin/bash -eux

if [[ ! "$ELIXIR" =~ ^(true|yes|on|1|TRUE|YES|ON])$ ]]; then
	exit
fi

echo "==>Starting the Elixir/Erlang script"
erpm=erlang-solutions-1.0-1.noarch.rpm
wget -q https://packages.erlang-solutions.com/${erpm}
rpm -Uvh ${erpm}
rm -f ${erpm}

echo "==>Installing Erlang"
yum install -y erlang

# get and build elixir
echo "==>Starting Elixir build from source"
elx_version=v1.3.2
elx=/tmp/elixir
cdir=${PWD}
mkdir ${elx}; \
git clone https://github.com/elixir-lang/elixir.git ${elx}; \
cd ${elx}; \
git checkout tags/${elx_version}; \
make clean; \
make; \
cd ${cdir}; \
mv -f ${elx} /usr/sbin/

echo "==>Installed Elixir version: ${elx_version}"

# add elixir to the path
ebash=/etc/profile.d/elixir.sh
echo "export PATH=/usr/sbin/elixir/bin:\$PATH" > $ebash
