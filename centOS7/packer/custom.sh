#!/bin/bash

set -x

echo "ADDING THE ERLANG RPMS..."
erpm=erlang-solutions-1.0-1.noarch.rpm
wget -q https://packages.erlang-solutions.com/${erpm}
rpm -Uvh ${erpm}
rm -f ${erpm}
echo "DONE ADDING THE ERLANG RPMS"

echo "STARTING YUM INSTALLS..."
yum install -y erlang
yum install -y erlang-hipe
echo "FINISHED THE YUM INSTALLS"

# install vagrant's key
echo "INSTALL THE VAGRANT SSH KEY..."
vssh="/home/vagrant/.ssh"
mkdir ${vssh}
curl https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub > ${vssh}/authorized_keys
chmod 0700 ${vssh}
chmod 0600 ${vssh}/authorized_keys
chown -R vagrant.vagrant ${vssh}
echo "FINISHED INSTALLING THE VAGRANT SSH KEY"

#get and install golang
echo "STARTING TO GET AND INSTALL GOLANG..."
go_version=1.6.2
gbash=/etc/profile.d/golang.sh
gsha256sum=e40c36ae71756198478624ed1bb4ce17597b3c19d243f3f0899bb5740d56212a

echo "${gsha256sum} golang.tar" > golang.tar-CHECKSUM
wget https://storage.googleapis.com/golang/go${go_version}.linux-amd64.tar.gz -q -O golang.tar
rslt=$(sha256sum -c golang.tar-CHECKSUM 2>&1 | grep OK | wc -l)

if [ ${rslt} = 1 ]; then
	tar -C /usr/local -xzf golang.tar
	echo "ADDING GOLANG PATH TO USER PATH..."
	echo "CREATING etc/profile.d/golang.sh"
	echo "export PATH=/usr/local/go/bin:\$PATH" > $gbash
	echo "FINISHED INSTALLING GOLANG"
else
	echo "golang sha256 did not match, skipping golang install"
fi
rm -f golang.tar

# get and build elixir
echo "STARTING TO GET AND BUILD ELIXIR..."
elx=/tmp/elixir
cdir=${PWD}
mkdir ${elx}; \
git clone https://github.com/elixir-lang/elixir.git ${elx}; \
cd ${elx}; \
git checkout tags/v1.2.6; \
make clean; \
make; \
cd ${cdir}; \
mv -f ${elx} /usr/sbin/

echo "FINISHED GETTING AND BUILDING ELIXIR"

echo "ADDING ELIXIR PATH TO USER PATH..."
echo "CREATING profile.d/elixir.sh"
# add elixir to the path
ebash=/etc/profile.d/elixir.sh
echo "export PATH=/usr/sbin/elixir/bin:\$PATH" > $ebash
echo "FINISHED CREATING profile.d/elixir.sh"
