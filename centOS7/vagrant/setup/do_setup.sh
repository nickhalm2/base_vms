#!/bin/bash
set -e 

cdir=$(pwd)
ret=false

echo $0
echo $username

getent passwd $username > /dev/null 2>&1 && ret=true
if [ $ret = false ]; then
	echo "Adding new user: $username"
	sudo useradd $username -G wheel,docker
	echo "Setting user $username password"
	sudo echo $username:$username | chpasswd
fi

if [ -d /vagrant/setup/ssh ]; then
	echo "Configuring ssh"
	cp -r /vagrant/setup/ssh/ /home/$username/.ssh
	chown -R $username:$username /home/$username/.ssh/
	chmod 700 /home/$username/.ssh/
	chmod 600 /home/$username/.ssh/*
fi

echo "Setting up .bash_profile"
cp -r /vagrant/setup/.bash_profile /home/$username/.bash_profile
chown -R $username:$username /home/$username/.bash_profile
