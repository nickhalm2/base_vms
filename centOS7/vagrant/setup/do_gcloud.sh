#!/bin/bash

set -x 

function add_gcloud() {
	echo "ADDING GCLOUD SDK"
	gcloudversion=117.0.0
	gcloudsha1=34a59db03cdc5ef7fc32ef90b98eaf9e4deff83f
	gcloudfile=gcloud.gz.tar

	echo "${gcloudsha1} ${gcloudfile}" > ${gcloudfile}-CHECKSUM
	wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${gcloudversion}-linux-x86_64.tar.gz -q -O ${gcloudfile}
	rslt=$(sha1sum -c ${gcloudfile}-CHECKSUM 2>&1 | grep OK | wc -l)

	echo "rslt = $rslt"

	if [ ${rslt} = 1 ]; then
		echo "SUCCESSFULLY DOWNLOADED GCLOUD SDK"
		echo "EXTRACTING GCLOUD SDK..."
		tar -xzf ${gcloudfile}
		echo "EXTRACTED GCLOUD SDK"
		echo "INSTALLING GCLOUD SDK..."
		./google-cloud-sdk/install.sh --usage-reporting false --command-completion true --additional-components alpha beta --quiet
		echo "DONE INSTALLING GCLOUD SDK"
	else
		echo "GCLOUD SDK DOWNLOAD DID NOT MATCH CHECKSUM. ABORTING."
	fi

	echo "DONE ADDING GCLOUD SDK"
}

add_gcloud
