#!/bin/bash

echo "user= $1"
sudo su $username << 'EOF'
echo "user home $HOME"

export DOTFILES=$HOME/dotfiles
echo "set dotfiles directory to $DOTFILES"
echo "creating dotfiles directory"
mkdir $DOTFILES

echo "git clone https://github.com/nhalm2/dotfiles.git $DOTFILES"
cd $DOTFILES

git clone --recursive https://github.com/nhalm2/dotfiles.git .

echo ""
echo ""
echo "run the dotfiles setup"
./initial_setup.sh
echo "finsihed setup"

EOF
