Vagrant Developer VM
====================
* * *

## What is this?

This is a vagrant script to make a developer virtual machine using [Vagrant](https://www.vagrantup.com/).  The virtuial machine is based on [CentOS 7](https://www.centos.org/) and includes many of the tools necessary to develop in our environment.

## What do I need to use this?

Currently we have virtual machines that support [Virtual Box](https://www.virtualbox.org/wiki/Downloads).  Virtual box is free and available for most environments.  We can create virtual machines to run using [VMware Fusion](http://www.vmware.com/products/fusion/) and [VMware Workstation Pro](http://www.vmware.com/products/workstation/) as needed.

## How do I use it?

Clone this repository into a directory that you want to hold the virtual machine disks.  You can then edit the `Vagrant` file and change the username that you would like to use.  By default the username and password are the same and can be configured in the `Vagrantfile` by putting a new value in the `username` environment variable configuration.  You can include a customized ssh configuration and keys in the `setup/ssh` directory.  They will be added to your new virtual machine and the user created.  Any files in the current directory will automatically be available to your virtual machine in the `/vagrant` directory.

After you have cloned the repository and made your changes run:
```
vagrant up
```

When you are done with your virtual machine do:
```
vagrant destroy
```

You can stop your virtual machine by issueing the command:
```
vagrant halt
```

You can run the provisioners at anytime be issuing:
```
vagrant provision
```

If you make changes to your Vagrant file that effect the network, CPU, or memory settings issue:
```
vagrant reload
```
